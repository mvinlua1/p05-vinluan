//
//  ViewController.h
//  MedicalApp
//
//  Created by Marvin Vinluan on 6/23/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *testLabel;
@property (strong, nonatomic) IBOutlet UIView *outsideView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedMaleFemale;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedHypertension;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedAge;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedDiabetes;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedCHF;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedStroke;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedVascularDisease;
@property (nonatomic) NSInteger score;
@property (nonatomic) NSArray *strokeRisks;
@property (strong, nonatomic) IBOutlet UILabel *riskLabel;
@property (strong, nonatomic) IBOutlet UILabel *warningLabel;
- (IBAction)updateScore;

@end

