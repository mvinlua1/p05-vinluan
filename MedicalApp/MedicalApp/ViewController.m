//
//  ViewController.m
//  MedicalApp
//
//  Created by Marvin Vinluan on 6/23/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.testLabel setText:@"CHA₂DS₂-VASc score: 0"];
    [self.riskLabel setText:@"Annual stroke risk: 0.2%"];
    [self.warningLabel setText:@""];
    // Risk percentages from Swedish Atrial Fibrillation Cohort Study
    // via https://www.mdcalc.com/cha2ds2-vasc-score-atrial-fibrillation-stroke-risk#evidence
    self.strokeRisks = @[@"0.2%",
                         @"0.6%",
                         @"2.2%",
                         @"3.2%",
                         @"4.8%",
                         @"7.2%",
                         @"9.7%",
                         @"11.2%",
                         @"10.8%",
                         @"12.2%"
                         ];
    self.score = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)updateScore {
    // using [segmentedXXX selectedSegmentIndex],
    // figure out the score value of each segmented control,
    // add them all up and save to 'score',
    // then update the score label to reflect new score value
    NSInteger temp_score = 0;
    temp_score += [self.segmentedMaleFemale selectedSegmentIndex];
    temp_score += [self.segmentedHypertension selectedSegmentIndex];
    temp_score += [self.segmentedAge selectedSegmentIndex];
    temp_score += [self.segmentedDiabetes selectedSegmentIndex];
    temp_score += [self.segmentedCHF selectedSegmentIndex];
    temp_score += [self.segmentedStroke selectedSegmentIndex] * 2;
    temp_score += [self.segmentedVascularDisease selectedSegmentIndex];
    [self.testLabel setText:[NSString stringWithFormat:@"CHA₂DS₂-VASc score: %ld",temp_score]];
    NSString *riskPreface = @"Stroke risk: ";
    [self.riskLabel setText:[riskPreface stringByAppendingString:self.strokeRisks[temp_score]]];
    if(temp_score > 1) {
        [self.warningLabel setText:@"Score over 1: Consider anticoagulant"];
    } else {
        [self.warningLabel setText:@""];
    }
}
@end
