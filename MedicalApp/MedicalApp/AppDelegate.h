//
//  AppDelegate.h
//  MedicalApp
//
//  Created by Marvin Vinluan on 6/23/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

