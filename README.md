# Medical App (Project 5)
Marvin Vinluan (mvinlua1@binghamton.edu)  
3 July 2017

Medical application to recreate the CHA2DS2-VASc calculator for annual stroke risk.

https://en.wikipedia.org/wiki/CHA2DS2%E2%80%93VASc_score

Doctors often use this scoring system to give a ballpark estimate of cardiovascular health, and to decide whether or not anticoagulant drugs are suggested in a given situation.

Normally, doctors would have to consult multiple charts, but this app puts all the calculations (and important information) in front of you instantly.

Built/tested on iPhone 6s.